var ast = require("./ast.js");
var apglib = require("apg-lib");
var apgApi = require("apg-api");

var parser = new apglib.parser();
parser.ast = new ast();
function newParser(){
parser = new apglib.parser();
parser.ast = new ast();
attachCallbacks(parser.ast.callbacks, {
validJSON: (string)=>(string),
variable: (string)=>string,
path: (string)=>string,
insert: (string)=>string,
assignment: (string)=>string,
lookup: (string)=>string,
comparison: (string)=>string,
dereference: (string)=>string,
queryOr: (string)=>string,
queryAnd: (string)=>string,
queryRule: (string)=>string,
pipes: (string)=>string,
pipe: (string)=>string,
fields: (string)=>string,
field: (string)=>string,
function: (string)=>string,
arguments: (string)=>string,
fieldAssign: (string)=>string,
mathOpAd: (string)=>string,
mathOpMu: (string)=>string,
mulBlock: (string)=>string,
addBlock: (string)=>string,

});
}
// customer|length{contact{name,ok},id}
var id = apglib.ids;
var grammar = `
jsoqs = assignment / insert / lookup;
assignment = path "=" 0*1(validJSON / lookup) 
insert = path "<=" 0*1(validJSON / lookup)
lookup = path [queryParathesis] [pipes] [fields]
fields = "{" 0*(" ") *((fieldAssign / field) 0*(" ") "," 0*(" ")) (fieldAssign / field) 0*(" ") "}"
fieldAssign = variable ":" 0*(" ") (mathGroup / mathParathesis / function / field)
field = variable [queryParathesis] [pipes] [fields]
function = variable arguments
arguments = "(" 0*(" ") *((validJSON / mathGroup / mathParathesis / variable) 0*(" ") "," 0*(" ")) (validJSON / mathGroup / mathParathesis / variable) 0*(" ") ")"
pipes = "<" pipe *("|" *(" ") pipe) ">"
pipe = variable [pipeArg]
pipeArg = ":" *(" ") validJSON
mathParathesis = "(" mathGroup ")";
mathGroup = addBlock / mulBlock;
addBlock = (mathParathesis / mulBlock / function / variable) mathOpAd (mathParathesis / function / rightMath)
mulBlock = (mathParathesis / function / variable) mathOpMu (mathParathesis / function / variable)
mathOpAd = "+" / "-"
mathOpMu = "/" / "*"
rightMath = mathGroup / variable;
path = variable *(dereference variable);
dereference = 0*1( queryParathesis) [pipes] ".";
queryParathesis = "[" queryAndOr "]";
queryAndOr = queryOr / queryAnd;
queryAnd = 0*1(*(queryRule ",") queryRule);
queryOr = "(" queryAndOr ")" "|" "(" queryAndOr ")";
queryRule = variable *(dereference variable) comparison validJSON;
variable = 1*(validJavascriptVariableCharacter);
validJavascriptVariableCharacter = %x41-5A / %x61-7A / %x30-39 / "_";
comparison = ">" / "<" / "=" / ":" / ">=" / "<=" / "==" / "===" / "~" / "#" / "{" /  "!=" / "!~" / "!{";
validJSON = ws value ws;


begin-array     = ws %x5B ws  ; [ left square bracket;
begin-object    = ws %x7B ws  ; { left curly bracket;
end-array       = ws %x5D ws  ; ] right square bracket;
end-object      = ws %x7D ws  ; } right curly bracket;
name-separator  = ws %x3A ws  ; : colon;
value-separator = ws %x2C ws  ; , comma 

ws = *(
      %x20 /              ; Space
      %x09 /              ; Horizontal tab
      %x0A /              ; Line feed or New line
      %x0D )              ; Carriage return

value = false / null / true / object / array / number / string
false = %x66.61.6c.73.65   ; false
null  = %x6e.75.6c.6c      ; null
true  = %x74.72.75.65      ; true

object = begin-object [ member *( value-separator member ) ]
       end-object
member = string name-separator value

array = begin-array [ value *( value-separator value ) ] end-array

number = [ minus ] int [ frac ] [ exp ]
decimal-point = %x2E       ; .
digit1-9 = %x31-39         ; 1-9
e = %x65 / %x45            ; e E
exp = e [ minus / plus ] 1*%x30-39
frac = decimal-point 1*%x30-39
int = zero / ( digit1-9 *%x30-39 )
minus = %x2D               ; -
plus = %x2B                ; +
zero = %x30                ; 0
quotation-mark = %x22      ; "
unescaped = %x20-21 / %x23-5B / %x5D-10FFFF
string = quotation-mark *char quotation-mark
char = unescaped /
  escape (
      %x22 /          ; "    quotation mark  U+0022
      %x5C /          ; \    reverse solidus U+005C
      %x2F /          ; /    solidus         U+002F
      %x62 /          ; b    backspace       U+0008
      %x66 /          ; f    form feed       U+000C
      %x6E /          ; n    line feed       U+000A
      %x72 /          ; r    carriage return U+000D
      %x74 /          ; t    tab             U+0009
      %x75 4(%x30-39 / "A" / "B" / "C" / "D" / "E" / "F") )  ; uXXXX                U+XXXX
escape = %x5C              ; \
`;
let apiObj = new apgApi(grammar)
apiObj.generate();
if (apiObj.errors.length) {
logErrors(apiObj, "GRAMMAR ERRORS");
throw new Error("grammar has errors");
}
else{
  //console.log("Grammar Valid");
}
var grammarObj = apiObj.toObject();
module.exports = function(jsoqsExpression){
  newParser();
  //console.log(grammarObj, jsoqsExpression);
  let result = parser.parse(grammarObj, 'jsoqs', jsoqsExpression);
  if (result.success===false){
    throw Error(`Syntax Error '${jsoqsExpression[result.maxMatched]}' not recognized at character ${result.maxMatched+1}. JSOQ is invalid.`);
  }
  let ast = toObject(parser.ast);
  //console.log(jsoqsExpression, JSON.stringify(ast, null, 2));
  this.ast = ast;
  checkAstPipeOrder(this.ast[0]);
  this.getAst = function(){
    return this.ast[0];
  }
  this.exec = function(obj){
      if (obj instanceof Array){
        var object = Object.assign([],obj);
      }
      else{
        var object = Object.assign({},obj);
      }
  let returnObject = false;
      for (let i=0; i<this.ast.length; i++){
        if (this.ast[i].name == "dereference"){
            if ((this.ast[i].value.length === 1) && !(object instanceof Array)){
              object = object[this.ast[i+1].value];
            }
            else{
              if (object instanceof Array){
                returnObject = true;
                this.ast[i].children.map(function(rule){
                  let attribute = rule.children[0];
                  let comparison = rule.children[1];
                  let validJSON = rule.children[2];
                  object = object.filter((item)=>{
                    if (comparison == ">"){
                      return dereference(item, attribute.split(".")) > JSON.parse(validJSON);
                    }
                    if (comparison == "<"){
                      return dereference(item, attribute.split(".")) < JSON.parse(validJSON);
                    }
                    if (comparison == ">="){
                      return dereference(item, attribute.split(".")) >= JSON.parse(validJSON);
                    }
                    if (comparison == "<="){
                      return dereference(item, attribute.split(".")) <= JSON.parse(validJSON);
                    }
                    if (comparison == "="){
                      return dereference(item, attribute.split(".")) === JSON.parse(validJSON);
                    }
                    if (comparison == "=="){
                      return dereference(item, attribute.split(".")) == JSON.parse(validJSON);
                    }
                    if (comparison == "==="){
                      return dereference(item, attribute.split(".")) === JSON.parse(validJSON);
                    }
                  });
                });
                if (this.ast[i].value[this.ast[i].value.length-1] === "."){
                    let scope = this;
                    object = object.map((item)=>{
                      return item[scope.ast[i+1].value];
                    });
                }
                if (this.ast[i].value[this.ast[i].value.length-1] === "]"){
                    let scope = this;
                    object = [].concat(...object);
                }
              }
              else{
                throw Error("Attempt to dereference non-array value: "+this.ast[i-1].value);
              }
          }
        }
      }
      if ((object.length === 1) && !returnObject){
        return object[0];
      }
      else{
        return object;
      }
   }
}
function checkAstPipeOrder(node){
  if (node.name==="pipes"){
    checkPipeOrder(node.children)
  }
  else if (node.name==="function"){
    checkFunctionValid(node.children[0], node.children[1])
  }
  else{
    for (let item of node.children){
      checkAstPipeOrder(item)
    }
  }
  
}
function checkFunctionValid(func, args){
  let funcs = ["max","min", "sum", "avg","gt","lt","gte","lte","eq","neq","floor","ceil","ln","exp","sqrt","pow","log","cond"];
  if (funcs.indexOf(func.value)===-1){
    throw new Error(`'${func.value}' is not a valid function.`)
  }
}

function checkPipeOrder(pipes){
  /**
 * def
 * math
 * group
 * aggregate: min, max, sum, avg, math, keys
 * def?
 * sort
 * skip
 * limit
 * keys
 * {min: {field: "ok", as: "what"}, }
 */
  pipes = pipes.map((item)=>item.children[0].value);
  let order = ["def","group","sort","skip","limit"];
  let index = 0;
  for (let pipeName of pipes){
    let j = order.indexOf(pipeName);
    if (j!==-1){
      if (j < index){
        throw new Error(`Pipes in improper order. '${order[j]}' cannot follow '${order[index]}'.`)
      }
      index = j;
    }
    else{
      throw new Error(`Invalid pipe '${pipeName}'`)
    }
  }
}
function toObject(ast){
  var display = String.fromCharCode;
  var obj = {children:[]};
  var depth = 0;
  var objStack = [];
  ast.records.forEach(function(rec, index) {
    if (rec.state === id.SEM_PRE) {
      obj.children.push({name:rec.name, value: display(...ast.chars.slice(rec.phraseIndex,rec.phraseIndex+rec.phraseLength)), children: []});
      objStack.push(obj);
      obj = obj.children[obj.children.length-1];
    } else if (rec.state === id.SEM_POST) {
      obj = objStack.pop();
    }
    else{
      throw Error("This ain't no tree");
      obj.children.push({name:rec.name, value: display(chast.charsars.slice(rec.phraseIndex,rec.phraseIndex+rec.phraseLength)), children: []});
    }
  });
  return obj.children;
}
function attachCallbacks(callbacks, object){
  for (name in object){
    callbacks[name] = function (state, chars, phraseIndex, phraseLength, data){
      var ret = id.SEM_OK;
      if (state == id.SEM_PRE) {
          data[name] = callbacks[name](apglib.utils.charsToString(chars, phraseIndex, phraseLength));
      }
      else if (state == id.SEM_POST) {
        /* not used in this example */
      }
      return ret;
    }
  }
}
function dereference(obj, args){
   for (var i=0;i<args.length;i++){
      obj = obj[args[i]];
   }
   return obj;
}
function logErrors(api, header){
  console.log("\n");
  console.log(header + ":");
  console.log(api.errorsToAscii());
  console.log("\nORIGINAL GRAMMAR:");
  console.log(api.linesToAscii());
}
